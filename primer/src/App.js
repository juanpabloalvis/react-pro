import React, {Component} from 'react';

const msg = "Esto es una constante";
const cuenta = 4;

function esPar(){
	return cuenta % 2 === 0 ? "Par" : "Impar";
}

class App extends Component{

  constructor(props){
  	super(props);
  	this.state = {
  		count:3
  	}
  }
  
  isEven(val){
	return val % 2 === 0 ? "Par" : "Impar";
  }
 getClassName(val){
 	return val %2 === 0
 	   ? "bg-primary text-white text-center p-2 m-1"
 	   : "bg-secondary text-white text-center p-2 m-1"
 }
 handelClick = () => this.setState({count: this.state.count + 1})

  render = () =>
  <div>
    <h4 className="bg-primary text-white text-center p-2 m-1">
    {msg}, número de cosas: {this.state.count}, { esPar()} 
    </h4>
    <button className="btn btn-info m-2" onClick={this.handelClick}>Click Me</button>
    <h4 className={this.getClassName(this.state.count)}>
     isEven(): { this.isEven(this.state.count)} 
    </h4>
    </div>
}

export default App;
