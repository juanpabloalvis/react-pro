import {sumValues} from './sum.js';


// SIN PROMESA
 export function asyncAddSinPromesa(values){
    setTimeout(()=>{
     let total = sumValues(values);
     console.log(`Async Total: ${total}`);
     return total;
    }, 1500);
 }


// CON PROMESA
export function asyncAddConPromesa(values){
    return new Promise(callback =>
    setTimeout(() => {
         let total = sumValues(values);
         console.log(`Async Total: ${total}`);
         callback(total);

    }, 500));
}