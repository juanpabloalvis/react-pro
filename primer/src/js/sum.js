
// export default function(values){
//     return values.reduce((total, val) => total +val, 0);
// }

export function sumValues (values){
    return values.reduce((total, val) => total +val, 0);
}

export default function sumOdd (values){
    return values.filter((x,index) => x%2 === 0).reduce((total, val) => total +val, 0);
}