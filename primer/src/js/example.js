import otraFn, {sumValues} from './sum.js';
import {multiply, subtract} from './operation.js';
import {asyncAddSinPromesa, asyncAddConPromesa} from './async.js';


console.log("Hola");

const myFunct0 = function(){
    console.log("Esto es una sentencia.");
}

myFunct0();
function myFunct(name, weather, ...extraArgs){
    console.log("Hola "+name+".");
    console.log("El clima hoy es "+weather+"");

    for (let i = 0; i < extraArgs.length; i++){
        console.log("Extra Arg: "+extraArgs[i]);
    }
}

myFunct("Juan", "Soleado", "Uno", "dos", "tres");

const myFunct1 = (name)=>{
    return("Hola "+name+".")
}

console.log(myFunct1("Jp"));


// Funcion que recibe una funcion
const myFunct2 = (aFunction)=>{
    return("Oi "+aFunction()+".")
}

console.log(myFunct2(function(){
    return "Joao";
}));

// Lo mismo pero encadenado
function printName(aFunction, printFunction){
    printFunction(myFunct2(aFunction));
}

printName(function () { return "Juan Pablo"}, console.log)

// Los mismo pero con arrow function
const myFunct3 = (aFunction) => ("Hello "+aFunction()+".");
const printNameFat = (aFunctionFat, printFunctionFat) =>
    printFunctionFat(myFunct3(aFunctionFat))

printNameFat(function(){return "JP Alvis"}, console.log)

//closure: Si definio una funcion dentro de otra funcion, la funcion interna se puede 
// acceder a las variables que están afuera de la funcion externa, con una caracteristica llamada closure
// esto significa que no necesitamos definir parametrosen las funciones interna para pasar valores.
function myClosure(name){
    let myLocalVar = "sunny";
    let innerFunction = function (){
        return ("hello "+ name + "Today is "+myLocalVar);

    }
    return innerFunction();
}
console.log(myClosure("Juan"));

// Utilizando plantillas
function plantillaMsg(contenido){
    let unMensaje = `Estes es un mensaje: ${contenido}`;    

    console.log(unMensaje);
}

plantillaMsg("abckded");

let num = 5;
let num2 = 5.7621;
console.log("5"+num.toString());
console.log(num2.toFixed(2));
console.log(num2.toPrecision(3));

// Arrays
// let myArray = new Array();
// myArray[0] = 1;
// myArray[1] = "Juan P";
// myArray[2] = true;
let myArray = [1, "Juan P", true];
console.log(`Index 1: ${myArray[1]}`);
myArray.forEach((x, index) => console.log(`Indice ${index} valor: ${x}`));
console.log("---");

//sprad operador
const printItems =  (param1, param2, param3) => {
    console.log(`param1 ${param1}`);
    console.log(`param2 ${param3}`);
    console.log(`param3 ${param3}`);
}

printItems(...myArray);
console.log("---");
let myArray2 = [...myArray, 2, "Alvis", false];
myArray2.forEach((x, index) => console.log(`Indice ${index} valor: ${x}`));

console.log(myArray2.join("|"));
console.log(myArray2.pop());
console.log(myArray2.join("|"));
myArray2.push("otro");
console.log(myArray2.sort().reverse().filter(item => item !== 2).join("|"));


let myData = {
    name: "Joao",
    bornDate: "2010-10-10",
    sport: "Cycling",
    printData: function(){
        console.log(`${myData.name} parctica ${myData.sport} y nacio en ${myData.bornDate}. `);
    }

};


let myData2 = {
    name: "Joao",
    bornDate: "2010-10-10",
    sport: "Cycling",
    printDatatwo: ()=> {
        console.log(`${myData2.name} practices ${myData2.sport} y was born in ${myData2.bornDate}. `);
    }

};

myData.printData();
myData2.printDatatwo();


console.log("############################# CLASES ############################");


class MyClass {
    constructor(){
        this.name = "Joao Pablo";
        this.sport = "Cycling2";
    }

    printMessage = () => {
        console.log(`${this.name} practices ${this.sport} `);
    }
}

let myInstance = new MyClass();
myInstance.printMessage();

let secondObject = {};

Object.assign(secondObject, myInstance);
secondObject.printMessage();

let thirdObject = {...secondObject, clima:"Soleado"};
console.log(`Mis datos: deporte-->${thirdObject.sport} nombre-->${thirdObject.name} clima-->${thirdObject.clima}`);

const myData3 = {
    name: "Jp",
    location: {
        city: "Cogua",
        country: "France"
    },
    employment: {
        title: "CTO",
        dept: "Finance"
    }
}

function printDataNice({name, location:{city}, employment:{title}}){
    console.log(`Datos nice: Nombre-->${name} Ciudad-->${city} Puesto-->${title}`);
}

printDataNice(myData3);

let values = [10,45,123,1,34,55];
let total = sumValues(values);
console.log(`Total: ${total}`);

console.log(otraFn(values));

console.log(`Multiply: ${multiply(values)}`);
console.log(`Subtract: ${subtract(645, values)}`);


let totAsync = asyncAddSinPromesa(values);
console.log(`Total principal sin promesa: ${totAsync}`)


asyncAddConPromesa(values).then(total => console.log(`Total principal con promesa: ${total}`));

// que es igual:
async function doTask(){
    let total = await asyncAddConPromesa(values);
    console.log(`Total async-await con promesa: ${total}`)
}
doTask();
