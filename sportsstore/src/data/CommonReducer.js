// CommonReducers toma una lista de funciones reducers: p.e.: ShopReducer, CartReducer y los que agregemos. 
// y a cada reducer le aplica los argumentos que vienen en los parámetros storeData y action(que es otra funcion), 
// finalmente retorna un nuevo store

export const CommonReducer = (...reducers) => (storeData, action) => {
    for (let i = 0; i < reducers.length; i++) {
        
        let newStore = reducers[i](storeData, action);
        if (newStore !== storeData) {
            return newStore;
        }
    }
    return storeData;
};