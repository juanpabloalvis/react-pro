import {applyMiddleware, createStore} from "redux";
import {ShopReducer} from "./ShopReducer";
import {CartReducer} from "./CartReducer";
import {CommonReducer} from "./CommonReducer";
import {asyncActions} from "./AsyncMiddleware";

// applyMiddleware se utliza para envolver el middlewer así recibe los 'actions' y el resultatod es pasado como 
// argumenteo a la funcion 'createStore' que crea el datastore.
// Así las funciones creadas en AsyncMiddleware.js podrán inspeccionar todas las ccionene enviadas al datastor 
// y de las misma manrea tratar con la carga de las 'Promises'
export const SportsStoreDataStore = createStore(CommonReducer(ShopReducer, CartReducer),
        applyMiddleware(asyncActions));

